package INF101.lab2;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge  implements IFridge{
    ArrayList <FridgeItem> items= new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return items.size();
        }
    
    int max_size =  20;
    @Override
    public int totalSize() {
        // TODO Auto-generated method stub

        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (nItemsInFridge() < totalSize()){
            items.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (nItemsInFridge() >0){
            items.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        items.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        ArrayList<FridgeItem> goodFood = new ArrayList<>();
        for (FridgeItem food : items){
            if (food.hasExpired()){
                expiredFood.add(food);
            }
            else{
                goodFood.add(food);
            }
        }
        items.clear();
        items.addAll(goodFood);
        return expiredFood;
    }
    
}
